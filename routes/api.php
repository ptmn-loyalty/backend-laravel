<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/login', 'Auth\LoginController@jwtLogin')->name('jwt-login');
Route::post('/register', 'Auth\RegisterController@apiRegister')->name('api-register');
    
Route::middleware(['custom.jwt.auth'])->group(function () {
    Route::get('/refresh-token', 'CustomerController@refreshJwtToken')->name('refresh-jwt-token');
    Route::get('/my/point', 'CustomerController@myPoint')->name('my-point');
    Route::get('/my/voucher', 'CustomerController@myVoucher')->name('my-voucher');
    Route::get('/my/history', 'CustomerController@myHistory')->name('my-history');
    
    Route::get('/vouchers', 'VoucherController@index')->name('vouchers');
    Route::post('/point/claim', 'TransactionController@readQrCode')->name('read-qr-code');
    Route::post('/voucher/{id}/buy', 'VoucherController@buyVoucher')->name('buy-voucher');
    Route::post('/voucher/{id}/redeem', 'CustomerController@redeemVoucher')->name('redeem-voucher');
});

// debug
Route::get('/spbus', 'ReimbursementController@getSpbus');
Route::get('/transactions_histories', 'TransactionHistoryController@getTransactions');
Route::get('/users_vouchers', 'TransactionHistoryController@getUsersVouchers');
Route::get('/user_points', 'TransactionHistoryController@getUsers')->name('get-users');
Route::get('/vouchers_transactions', 'TransactionHistoryController@getVouchersTransactions');
// Route::post('/trx_to_points', 'TransactionController@readQrCode')->name('read-qr-code');