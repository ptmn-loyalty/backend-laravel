<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::middleware(['auth'])->group(function () {
            
});

Route::get('/user_verification/{email}/{verification_code}', 'Auth\RegisterController@verifyUser')->name('verify-user');

Route::middleware(['auth.roles:administrator'])->group(function () {
    // page
    Route::get('/transactions', 'TransactionHistoryController@index')->name('transactions');
    Route::get('/users', 'TransactionHistoryController@indexUser')->name('users');
    Route::get('/points', 'TransactionHistoryController@points')->name('points');        
    Route::get('/reimbursements', 'ReimbursementController@index')->name('reimbursements');    
    Route::get('/spbu', 'ReimbursementController@indexSpbu')->name('spbu');
    // datatables
    Route::get('/transactions_histories', 'TransactionHistoryController@getTransactions')->name('get-transactions');
    Route::get('/user_points', 'TransactionHistoryController@getUsers')->name('get-users');
    Route::get('/users_vouchers', 'TransactionHistoryController@getUsersVouchers')->name('get-users-vouchers');
    Route::get('/vouchers_transactions', 'TransactionHistoryController@getVouchersTransactions')->name('get-vouchers-transactions');
    Route::get('/spbu_vouchers', 'ReimbursementController@getSpbus')->name('get-spbus');
});

Route::middleware(['auth.roles:administrator|operator'])->group(function () {
    // Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/', 'QrcodeController@index')->name('root');
    Route::get('/home', 'QrcodeController@index')->name('home');
    Route::get('/change_password', 'HomeController@changePassword')->name('change-password');
    Route::post('/change_password', 'HomeController@updatePassword')->name('update-password');
    Route::get('/qr-generator', 'QrcodeController@index')->name('qr-generator');
    Route::post('/qr-generator', 'QrcodeController@generateQrCode')->name('generate-qr-code');
    Route::get('/spbus/find', 'SpbuController@find')->name('spbus-find');
    Route::get('/products/find', 'ProductController@find')->name('products-find');
});
