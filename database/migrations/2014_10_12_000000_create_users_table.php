<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('spbu_id')->nullable();
            $table->string('name');
            $table->string('email')->unique();
            $table->string('facebook_id')->nullable();
            $table->string('password')->nullable();
            $table->string('verification_code')->nullable();
            $table->boolean('is_verified')->default(false);            
            $table->integer('role_id')->unsigned()->default(0)->comment('0: customer, 1: operator, 2: admin');
            $table->decimal('point')->unsigned()->default(0);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
