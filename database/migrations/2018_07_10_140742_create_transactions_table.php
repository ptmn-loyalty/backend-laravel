<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('spbu_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->integer('user_voucher_id')->nullable();            
            $table->decimal('amount');
            $table->bigInteger('original_price');
            $table->bigInteger('discount_price')->default(0);
            $table->bigInteger('final_price');
            $table->datetime('generated_at');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('spbu_id')->references('id')->on('spbus');
            $table->foreign('product_id')->references('id')->on('products');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
