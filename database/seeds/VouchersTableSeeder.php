<?php

use Illuminate\Database\Seeder;

use App\Product;
use App\Voucher;
class VouchersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = Product::all();

        foreach($products as $product) {
            $voucher = new Voucher();
            $voucher->product_id = $product->id;
            $voucher->discount_price = 50000;
            $voucher->description = 'Potongan harga sebesar Rp '.number_format($voucher->discount_price,0,",",".").' untuk pembelian '. $product->name;
            $discount_price = $voucher->discount_price / 1000;
            $voucher->name = $product->name.' - '. $discount_price.'K';
            $voucher->points_required = 45;
            $voucher->save();
        }
    }
}
