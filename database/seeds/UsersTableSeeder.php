<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;
use League\Csv\Reader;
use League\Csv\Statement;

use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {        
        $path = ENV('CSV_PATH');
        $csv = Reader::createFromPath($path.'users.csv', 'r');
        $csv->setHeaderOffset(0); //set the CSV header offset

        $stmt = (new Statement());        
        $records = $stmt->process($csv);

        foreach ($records as $record) {
            $user = new User();
            $user->name = $record['name'];
            $user->email = $record['email'];
            $user->password = bcrypt($record['password']);
            $user->is_verified = $record['is_verified'];
            $user->role_id = $record['role_id'];
            if($record['spbu_id'] > 0) {
                $user->spbu_id = $record['spbu_id'];
            }
            $user->save();
        }
    }
}
