<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon;
use App\Transaction;
use App\Product;
use App\User;

class TransactionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $point_price = 17000;            
        for($i = 0; $i <= 100; $i++) {
            $trx = [
                'user_id' => rand(27, 39),
                'spbu_id' => rand(1, 25),
                'product_id' => rand(1, 26),
            ];
            
            $trx['amount'] = rand(1, 10);
            $trx['original_price'] = Product::find($trx['product_id'])->price * $trx['amount'];
            $trx['final_price'] = $trx['original_price'];
            $trx['generated_at'] = Carbon::now()->subSeconds(61);
    
            $temp = Transaction::where('spbu_id', $trx['spbu_id'])
                            ->where('product_id', $trx['product_id'])
                            ->where('original_price', $trx['original_price'])
                            ->where('generated_at', $trx['generated_at'])
                            ->first();
            if(empty($temp)) {
                Transaction::create($trx);
                $point = round($trx['final_price'] / $point_price, 2);
                $user = User::find($trx['user_id']);
                $user->point += $point;
                $user->save();
            }
        }        
    }
}
