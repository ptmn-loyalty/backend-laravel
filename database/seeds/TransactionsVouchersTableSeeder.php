<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon;
use App\Voucher;
use App\Product;
use App\Transaction;
use App\User;

class TransactionsVouchersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $i = 0;
        while($i <= 50) {
            $user_id = rand(27, 39);
            $user_voucher = DB::table('users_vouchers')
                            ->join('vouchers','users_vouchers.voucher_id','=','vouchers.id')
                            ->where('user_id', $user_id)
                            ->where('is_used', 0)
                            ->select('users_vouchers.*','vouchers.product_id','vouchers.discount_price')
                            ->first();
            
            if(!empty($user_voucher)) {
                $trx =  [
                    'user_id' => $user_id,
                    'spbu_id' => rand(1, 25),
                    'product_id' => $user_voucher->product_id,
                    'user_voucher_id' => $user_voucher->id
                ];

                $trx['amount'] = rand(1, 10);
                $trx['original_price'] = Product::find($trx['product_id'])->price * $trx['amount'];
                $trx['discount_price'] = $user_voucher->discount_price;
                $trx['final_price'] = max($trx['original_price'] - $trx['discount_price'], 0);
                $trx['generated_at'] = Carbon::now()->subSeconds(61);

                Transaction::create($trx);
                DB::table('users_vouchers')->where('id', $user_voucher->id)
                ->update([
                    'is_used' => true
                ]);                    
                $i++;
            }
        }   
    }
}
