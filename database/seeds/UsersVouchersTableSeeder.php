<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon;
use App\User;
use App\Uservoucher;
use App\Voucher;

class UsersVouchersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $point_required = 45;            
        for($i = 0; $i <= 100; $i++) {
            $user_id = rand(27, 39);
            $user = User::find($user_id);
            if($user->point > $point_required) {
                $voucher_id = rand(1, 25);                    
                $voucher_user = new Uservoucher();
                $voucher_user->user_id = $user_id;
                $voucher_user->voucher_id = $voucher_id;
                $voucher_user->valid_until = Carbon::now()->addMonths(12);
                $voucher_user->save();
                $user->point = $user->point - $point_required;
                $user->save();
            }
        }
    }
}
