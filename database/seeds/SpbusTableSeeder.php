<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;
use League\Csv\Reader;
use League\Csv\Statement;

use App\Spbu;

class SpbusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = ENV('CSV_PATH');
        $csv = Reader::createFromPath($path.'spbus.csv', 'r');
        $csv->setHeaderOffset(0); //set the CSV header offset

        $stmt = (new Statement());        
        $records = $stmt->process($csv);

        foreach ($records as $record) {
            Spbu::create($record);
        }
    }
}
