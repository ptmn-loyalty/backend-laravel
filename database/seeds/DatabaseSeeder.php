<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(SpbusTableSeeder::class);
        $this->call(ProductsTableSeeder::class);
        $this->call(VouchersTableSeeder::class);
        $this->call(TransactionsTableSeeder::class);
        $this->call(UsersVouchersTableSeeder::class);
        $this->call(TransactionsVouchersTableSeeder::class);
    }
}
