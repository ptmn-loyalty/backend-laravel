<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
use Tymon\JWTAuth\Exceptions\JWTException;

use App\User;
use App\Spbu;
use App\Product;
use App\Transaction;
use Carbon\Carbon;
use JWTAuth;
use Datatables;
use DB;

class TransactionHistoryController extends Controller
{
    protected $point_price = 17000;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transactions = DB::table('users')
                            ->join('transactions', 'users.id', '=', 'transactions.user_id')
                            ->join('products','products.id','=','transactions.product_id')
                            ->select('*')
                            ->get();
        return view('pages.transactions')->with([
            'transactions' => $transactions
        ]);
    }

    public function indexUser()
    {
        return view('pages.users');
    }

    public function points() {
        return view('pages.points');
    }

    public function getTransactions()
    {
        $transactions = DB::table('users')
                            ->join('transactions', 'users.id', '=', 'transactions.user_id')
                            ->join('products','products.id','=','transactions.product_id')
                            ->where('transactions.user_voucher_id','=', NULL)
                            ->select('users.email','users.name','transactions.*','products.name as product_name')
                            ->get();

        for($i = 0; $i < sizeof($transactions); $i++) {
            $transactions[$i]->no = $i + 1;
            $transactions[$i]->point = round($transactions[$i]->final_price / $this->point_price, 2);
            $transactions[$i]->formatted_price = number_format($transactions[$i]->final_price,0,",",".");
        }
        return Datatables::of($transactions)->make(true);
    }

    public function getUsers()
    {
        $items = User::where('role_id',0)->get();
        for($i = 0; $i < sizeof($items); $i++) {
            $items[$i]->no = $i + 1;
        }
        return Datatables::of($items)->make(true);
    }

    public function getUsersVouchers()
    {
        $items = DB::table('users')
                    ->join('users_vouchers', 'users.id', '=', 'users_vouchers.user_id')
                    ->join('vouchers','vouchers.id','=','users_vouchers.voucher_id')
                    ->join('products','products.id','=','users_vouchers.voucher_id')
                    ->select('users.email','users.name','products.name as product_name','vouchers.*','users_vouchers.*')
                    ->orderBy('users_vouchers.id')
                    ->get();
        
        $transactions = DB::table('transactions')
                            ->join('spbus', 'spbus.id', '=', 'transactions.spbu_id')
                            ->where('user_voucher_id','<>', null)
                            ->select('spbus.spbu_code','transactions.created_at','transactions.user_voucher_id')
                            ->get();

        for($i = 0; $i < sizeof($items); $i++) {
            $items[$i]->spbu_code = '';
            $items[$i]->used_at = '';
            $items[$i]->no = $i + 1;
            if($items[$i]->is_used) {
                $items[$i]->status = 'Sudah Dipakai';
                for($j = 0; $j < sizeof($transactions); $j++) {
                    if($items[$i]->id == $transactions[$j]->user_voucher_id) {
                        $items[$i]->spbu_code = $transactions[$j]->spbu_code;
                        $items[$i]->used_at = $transactions[$j]->created_at;
                    }
                }
            } else {
                $items[$i]->status = 'Belum Dipakai';
            }
        }
        return Datatables::of($items)->make(true);                            
    }

    public function getVouchersTransactions()
    {
        $transactions = DB::table('users')
                            ->join('transactions', 'users.id', '=', 'transactions.user_id')
                            ->join('products','products.id','=','transactions.product_id')
                            ->join('spbus','spbus.id','=','transactions.spbu_id')
                            ->where('transactions.user_voucher_id','<>', NULL)
                            ->select('users.email','users.name','transactions.*','products.name as product_name','spbus.spbu_code')
                            ->get();

        for($i = 0; $i < sizeof($transactions); $i++) {
            $transactions[$i]->no = $i + 1;
            $transactions[$i]->point = round($transactions[$i]->final_price / $this->point_price, 2);
            $transactions[$i]->formatted_price = number_format($transactions[$i]->final_price,0,",",".");
        }
        return Datatables::of($transactions)->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
