<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use DB;
use Auth;

use App\User;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function changePassword()
    {
        return view('auth.change-password');
    }

    public function updatePassword(Request $request)
    {
        $parameters = $request->only(
            'current_password',
            'password',
            'password_confirmation'
        );

        $validator = Validator::make($parameters, [
            'current_password' => 'required',
            'password' => 'required|string|min:6|confirmed'   
        ]);

        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator);
        }
        
        if (!(Hash::check($parameters['current_password'], Auth::user()->password))) {
            return redirect()->back()->with("error","Your current password does not matches with the password you provided. Please try again.");
        }

        $user = User::find(Auth::user()->id);
        $user->password = bcrypt($parameters['password']);
        $user->save();
        
        return redirect()->back()->with("success","Password changed successfully !");
    }
}
