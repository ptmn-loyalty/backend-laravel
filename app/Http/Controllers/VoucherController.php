<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;

use Carbon\Carbon;
use JWTAuth;
use App\Voucher;
use App\Uservoucher;
use DB;

class VoucherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $offset = (Input::get('start')) ? Input::get('start') : 0;
        $limit = (Input::get('limit')) ? Input::get('limit') : 100;
        $items = DB::table('vouchers')
                        ->offset($offset)
                        ->limit($limit)
                        ->get();

        $custom_items = [];
        for($i = 0; $i < sizeof($items); $i++) {
            $custom_items[$i] = (object) [
                'id' => $items[$i]->id,
                'name' => $items[$i]->name,
                'description' => $items[$i]->description,
                'availableUntil' => Carbon::now()->addMonths(12)->toDateTimeString(),
                'pointsNeeded' => $items[$i]->points_required
            ];
        }
        return response()->json(['status' => 200 ,'success' => true, 'data'=> [ 'points' => $custom_items ]], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function buyVoucher($id)
    {
        /*$parameters = $request->only('voucher_id');
        $validator = Validator::make($parameters, [
            'voucher_id' => 'required'
        ]);
        
        if($validator->fails())
        {
            return response()->json(['status' => 200 ,'success' => false, 'data'=> [ 'message' => (object) $validator->errors() ]], 200);
        }*/

        $user = JWTAuth::parseToken()->authenticate();
        $voucher = Voucher::find($id);
        if(empty($voucher)) {
            return response()->json(['status' => 200 ,'success' => false, 'data'=> [ 'message' => 'Voucher tidak tersedia' ]], 200);
        }
        if($user->point > $voucher->points_required) {                        
            $voucher_user = new Uservoucher();
            $voucher_user->user_id = $user->id;
            $voucher_user->voucher_id = $id;
            $voucher_user->valid_until = Carbon::now()->addMonths(12);
            $voucher_user->save();

            $user->point = $user->point - $voucher->points_required;
            $user->save();
            return response()->json(['status' => 200 ,'success' => false, 'data'=> [ 'message' => 'Voucher berhasil dibeli' ]], 200);
        }
        return response()->json(['status' => 200 ,'success' => false, 'data'=> [ 'message' => 'Point tidak mencukupi' ]], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
