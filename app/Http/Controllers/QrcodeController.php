<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

use App\Spbu;
use App\Product;
use App\Transaction;
use Carbon\Carbon;
use JWTAuth;
use Auth;

class QrcodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $spbus = Spbu::all();
        $products = Product::all();
        $user = Auth::user();
        $user->spbu = null;
        if($user->spbu_id > 0) {
            $user->spbu = Spbu::find($user->spbu_id);
        }
        return view('pages.qr-code-generator')->with([
            'spbus' => $spbus,
            'products' => $products,
            'user' => $user
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function generateQrCode(Request $request)
    {
        $parameters = $request->only(
            'spbu',
            'product',
            'amount'
        );
        $product = Product::find($parameters['product']);
        if(!is_numeric($parameters['amount'])) {
            return redirect()->back()->with([
                'error' => 'Invalid input. Jumlah Pembelian must be type of numeric.'
            ]);    
        }
        $total_price = $parameters['amount'] * $product->price;
        $result = $parameters['spbu'].'_'.$parameters['product'].'_'.$parameters['amount'].'_'.$total_price.'_'.Carbon::now();
        $encrypted_result = Crypt::encryptString($result);
        return redirect()->back()->with([
            'result' => $result,
            'encrypted_result' => $encrypted_result
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
