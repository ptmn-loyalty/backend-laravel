<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Spbu;
use Datatables;
use DB;

class ReimbursementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $spbus = Spbu::all();
        return view('pages.reimbursements')->with([
            'spbus' => $spbus
        ]);
    }

    public function indexSpbu()
    {
        return view('pages.spbus');
    }

    public function getSpbus()
    {
        $spbus = DB::table('spbus')
                    ->join('transactions','spbus.id','=','transactions.spbu_id')
                    ->where('transactions.user_voucher_id','<>',null)
                    ->select('spbus.spbu_code',
                            'spbus.address',
                            'spbus.city',
                            DB::raw('count(transactions.id) as voucher_count'))
                    ->groupBy('spbus.spbu_code','spbus.address','spbus.city')
                    ->get();

        for($i = 0; $i < sizeof($spbus); $i++) {
            $spbus[$i]->no = $i + 1;
        }
        return Datatables::of($spbus)->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
