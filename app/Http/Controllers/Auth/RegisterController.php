<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Http\Controllers\Controller;
use App\Mail\SendMailVerification;
use App\User;
use Mail;
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    public function apiRegister(Request $request)
    {
        $parameters = $request->only('email', 'name', 'password', 'password_confirmation');

        $rules = [
            'email' => 'bail|required|email|unique:users|max:255',
            'name' => 'required',
            'password' => 'required|string|min:6|confirmed'
        ];
        
        $validator = Validator::make($parameters, $rules);
        
        if($validator->fails()) {
            return response()->json(['success'=> false, 'error'=> $validator->messages()], 401);
        }
        
        $random_str = str_random(8);
        $verification_code = md5($parameters['email'].$random_str);
        
        $user = new User();
        $user->name = $parameters['name'];
        $user->email = $parameters['email'];
        $user->password = bcrypt($parameters['password']);
        $user->verification_code = $verification_code;
        $user->is_verified = 1;
        $user->save();

        // Mail::to($user->email)->send(new SendMailVerification($user));

        // return response()->json(['status' => 200 ,'success' => true, 'message'=> 'Register success, please check your email to verify your account.'], 200);

        return response()->json(['status' => 200 ,'success' => true, 'message'=> 'Register success, now you can login using your email and password.'], 200);
    }

    public function verifyUser($email, $verification_code)
    {
        $user = User::where('email', $email)
                    ->where('verification_code', $verification_code)
                    ->where('is_verified', 0)->first();
                
        if($user) {
            $user->is_verified = 1;
            $user->save();                    
            // return response()->json(['status' => 200 ,'success' => true, 'message' => 'Successfully verified'], 200);
            return view('pages.verify-user')->with([
                'class' => 'alert-success',
                'message' => 'Verification success. You may now login to your account.'
            ]);
        }
        
        // return response()->json(['status' => 200 ,'success' => false, 'message' => 'User not found or verification code is invalid'], 200);
        return view('pages.verify-user')->with([
            'class' => 'alert-warning',
            'message' => 'User not found or verification code is invalid.'
        ]);
    }
}
