<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
use Tymon\JWTAuth\Exceptions\JWTException;

use App\Spbu;
use App\Product;
use App\Transaction;
use Carbon\Carbon;
use JWTAuth;
use Datatables;
use DB;

class TransactionController extends Controller
{
    protected $user;
    protected $point_price = 17000;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            try {
                if($this->user = JWTAuth::parseToken()->authenticate()) {
                    return $next($request);
                } 
                return response()->json(['status'=> 401, 'message'=> 'Invalid token'], 401);    
            } catch(JWTException $e) {
                return response()->json(['status'=> 401, 'message'=> $e->getMessage()], 401);
            }
        });
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    public function getData()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function readQrCode(Request $request)
    {
        $parameters = $request->only('qrcode');
        try {
            $decrypted_qrcode = Crypt::decryptString($parameters['qrcode']);
            $qrcode = explode('_', $decrypted_qrcode);                            
            // return response()->json(['status'=> 200, 'success' => true, 'message'=> $qrcode], 200);
            if(sizeof($qrcode) >= 5) {
                $trx = [
                    'user_id' => $this->user->id,
                    'spbu_id' => $qrcode[0],
                    'product_id' => $qrcode[1],
                    'amount' => $qrcode[2],
                    'original_price' => $qrcode[3],
                    'final_price' => $qrcode[3],
                    'generated_at' => $qrcode[4]
                ];
                $temp = Transaction::where('spbu_id', $trx['spbu_id'])
                                ->where('product_id', $trx['product_id'])
                                ->where('original_price', $trx['original_price'])
                                ->where('generated_at', $trx['generated_at'])
                                ->first();
                if(empty($temp)) {
                    Transaction::create($trx);
                    $points = round($trx['final_price'] / $this->point_price, 2);
                    $this->user->point += $points;
                    $this->user->save();
                    return response()->json(['status'=> 200, 'success' => true, 'message'=> 'Congrats, you\'ve got '.$points.' points'], 200);
                }
            }
            return response()->json(['status'=> 200, 'success' => false, 'message'=> 'QR Code is already used'], 200);;
        } catch(DecryptException $e) {
            return response()->json(['status'=> 401, 'success' => false, 'message'=> 'Invalid input'], 401);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
