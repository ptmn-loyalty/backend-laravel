<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

use App\Spbu;
use App\Product;
use App\Transaction;
use Carbon\Carbon;
use JWTAuth;
use Datatables;
use DB;

class CustomerController extends Controller
{
    protected $user;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            try {
                if($this->user = JWTAuth::parseToken()->authenticate()) {
                    return $next($request);
                } 
                return response()->json(['status'=> 401, 'message'=> 'Invalid token'], 401);    
            } catch(JWTException $e) {
                return response()->json(['status'=> 401, 'message'=> $e->getMessage()], 401);
            }
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function myPoint()
    {
        return response()->json(['status' => 200 ,'success' => true, 'data'=> [ 'points' => $this->user->point ]], 200);
    }

    public function myVoucher(Request $request)
    {
        $offset = (Input::get('start')) ? Input::get('start') : 0;
        $limit = (Input::get('limit')) ? Input::get('limit') : 100;
        $items = DB::table('users_vouchers')
                        ->join('vouchers','users_vouchers.voucher_id','=','vouchers.id')
                        ->where('users_vouchers.user_id', $this->user->id)
                        ->select('users_vouchers.id as user_voucher_id','users_vouchers.*','vouchers.*')
                        ->offset($offset)
                        ->limit($limit)
                        ->get();
        $custom_items = [];
        for($i = 0; $i < sizeof($items); $i++) {
            $custom_items[$i] = (object) [
                'id' => $items[$i]->user_voucher_id,
                'name' => $items[$i]->name,
                'description' => $items[$i]->description,
                'expiredAt' => $items[$i]->valid_until
            ];
            $custom_items[$i]->status = 'available';
            if($items[$i]->is_used) {
                $custom_items[$i]->status = 'redeemed';
            }
            if(Carbon::now() >= $custom_items[$i]->expiredAt && !$items[$i]->is_used) {
                $custom_items[$i]->status = 'expired';
            }
        }
        return response()->json(['status' => 200 ,'success' => true, 'data'=> [ 'vouchers' => $custom_items ]], 200);
    }

    public function myHistory(Request $request)
    {
        $offset = (Input::get('start')) ? Input::get('start') : 0;
        $limit = (Input::get('limit')) ? Input::get('limit') : 100;
        $items = DB::table('users_vouchers')
                    ->join('products','products.id','=','users_vouchers.voucher_id')
                    ->join('vouchers','vouchers.id','=','users_vouchers.voucher_id')
                    ->where('users_vouchers.user_id', $this->user->id)
                    ->offset($offset)
                    ->limit($limit)        
                    ->select('products.*','vouchers.*','users_vouchers.*')
                    ->get();

        $transactions = DB::table('transactions')
                            ->join('spbus', 'spbus.id', '=', 'transactions.spbu_id')
                            ->where('transactions.user_id', $this->user->id)
                            ->where('user_voucher_id','<>', null)
                            ->select('spbus.spbu_code','transactions.created_at','transactions.user_voucher_id')
                            ->get();
        $custom_items = [];
        for($i = 0; $i < sizeof($items); $i++) {
            $custom_items[$i] = (object) [
                'productName' => $items[$i]->name,
                'merchantName' => '',
                'points' => $items[$i]->points_required,
                'dateTime' => $items[$i]->created_at
            ];
            for($j = 0; $j < sizeof($transactions); $j++) {
                if($items[$i]->id == $transactions[$j]->user_voucher_id) {
                    $custom_items[$i]->merchantName = $transactions[$j]->spbu_code;
                }
            }
        }
        return response()->json(['status' => 200 ,'success' => true, 'data'=> [ 'items' => $custom_items ]], 200);
    }

    public function redeemVoucher(Request $request, $id)
    {
        $parameters = $request->only('spbu_id','amount');
        $validator = Validator::make($parameters, [
            'spbu_id' => 'required|numeric',
            'amount' => 'required|numeric'
        ]);
        if($validator->fails())
        {
            return response()->json(['status' => 200 ,'success' => false, 'data'=> [ 'message' => (object) $validator->errors() ]], 200);
        }
        if(empty(Spbu::find($parameters['spbu_id']))) {
            return response()->json(['status' => 200 ,'success' => false, 'data'=> [ 'message' => 'Layanan belum tersedia pada SPBU ini' ]], 200);
        }
        
        $user_voucher = DB::table('users_vouchers')
                        ->join('vouchers','users_vouchers.voucher_id','=','vouchers.id')
                        ->where('user_id', $this->user->id)
                        ->where('users_vouchers.id', $id)
                        ->where('is_used', 0)
                        ->select('users_vouchers.*','vouchers.product_id','vouchers.discount_price')
                        ->first();
        
        if(!empty($user_voucher)) {
            $trx =  [
                'user_id' => $this->user->id,
                'spbu_id' => $parameters['spbu_id'],
                'product_id' => $user_voucher->product_id,
                'user_voucher_id' => $user_voucher->id
            ];

            $trx['amount'] = $parameters['amount'];
            $trx['original_price'] = Product::find($trx['product_id'])->price * $trx['amount'];
            $trx['discount_price'] = $user_voucher->discount_price;
            $trx['final_price'] = max($trx['original_price'] - $trx['discount_price'], 0);
            $trx['generated_at'] = Carbon::now()->subSeconds(61);

            Transaction::create($trx);
            DB::table('users_vouchers')->where('id', $user_voucher->id)
            ->update([
                'is_used' => true
            ]);
            return response()->json(['status' => 200 ,'success' => true, 'data'=> [ 'message' => 'Voucher berhasil digunakan' ]], 200);
        }
        return response()->json(['status' => 200 ,'success' => false, 'data'=> [ 'message' => 'Voucher tidak tersedia atau sudah digunakan' ]], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function refreshJwtToken()
    {
        $token = JWTAuth::getToken();
        if(!$token){
            return response()->json(['status' => 500, 'success' => false, 'error' => 'Failed to login, please try again.'], 500);
        }

        try {
            $token = JWTAuth::refresh($token);
        } catch(JWTException $e) {
            return response()->json(['status' => 500, 'success' => false, 'error' => 'Failed to login, please try again.'], 500);
        }

        return response()->json(['status' => 200 ,'success' => true, 'data'=> [ 'token' => $token ]], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
