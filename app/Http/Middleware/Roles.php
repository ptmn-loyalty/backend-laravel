<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Roles
{
    protected $authorizer;

    public function __construct(Auth $auth)
    {
        $this->authorizer = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $roles)
    {
        if($this->authorizer::check())
        {
            $user = $this->authorizer::user();        
            if($user->role_id > 0) {
                $scopes = array('customer', 'operator', 'administrator');
                if(!in_array($scopes[$user->role_id], explode('|', $roles)))
                {
                    return redirect('home');
                }
                return $next($request);    
            } else {
                $request->session()->invalidate();
                return redirect('login');
            }
        }
        return redirect('login');
    }
}
