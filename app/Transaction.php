<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable = array('user_id', 'spbu_id', 'product_id','voucher_id','original_price','discount_price','final_price','amount','generated_at');
}
