@extends('layouts.app')
@section('style')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">QR Code Generator</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('generate-qr-code') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            @if($user->role_id == 2)
                            <label for="spbu_list" class="col-md-4 control-label">Kode SPBU</label>
                            <div class="col-md-6">
                                <select id="spbu_list" name="spbu" class="form-control" required></select>
                            </div>
                            @else
                            <label for="spbu_list" class="col-md-4 control-label">Kode SPBU</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" value="{{$user->spbu->spbu_code}}" disabled>
                                <input type="hidden" name="spbu" class="form-control" value="{{$user->spbu->id}}">         
                            </div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="product_list" class="col-md-4 control-label">Product</label>
                            <div class="col-md-6">
                                <select id="product_list" name="product" class="form-control" required></select>
                            </div>                            
                        </div>
                        <div class="form-group">
                            <label for="amount" class="col-md-4 control-label">Jumlah Pembelian (Liter / Pack)</label>
                            <div class="col-md-6">
                                <input id="amount" type="text" class="form-control number-and-dot" name="amount" value="" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Generate
                                </button>
                            </div>
                        </div>
                    </form>                    
                </div>
            </div>

            @if(session()->has('encrypted_result'))                
            <div class="panel panel-default">
                <div class="panel-heading">QR Code</div>
                <div class="panel-body">
                    <div class="form-group">
                        <label for="qrcode" class="col-md-8 col-md-offset-2 control-label text-center">
                        <input type="hidden" value="{{session('encrypted_result')}}">
                        </label>
                    </div>
                    <div class="form-group">
                        <div class="col-md-4 col-md-offset-4 text-center">
                            {!! QrCode::size(200)->generate(session('encrypted_result')); !!}
                        </div>
                    </div>
                </div>
            </div>
            @endif
            @if(session()->has('error'))                
            <div class="panel panel-default">
                <div class="panel-heading">QR Code</div>
                <div class="panel-body">
                    <div class="form-group">
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>
</div>
@endsection
@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script type="text/javascript">
$(".number-and-dot").keydown(function (e) {
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl+A, Command+A
        (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
            // Allow: home, end, left, right, down, up
        (e.keyCode >= 35 && e.keyCode <= 40)) {
                // let it happen, don't do anything
                return;
    }
    
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }

    // debug
    // console.log("key code ", e.keyCode);
});
</script>
<script type="text/javascript">
    $('#spbu_list').select2({
        placeholder: "Pilih SPBU",
        minimumInputLength: 2,
        ajax: {
            url: '{{ route('spbus-find') }}',
            dataType: 'json',
            data: function (params) {
                return {
                    q: $.trim(params.term)
                };
            },
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache: true
        }
    })
</script>
<script>
    $('#product_list').select2({
        placeholder: "Pilih Produk",
        minimumInputLength: 2,
        ajax: {
            url: '{{ route('products-find') }}',
            dataType: 'json',
            data: function (params) {
                return {
                    q: $.trim(params.term)
                };
            },
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache: true
        }
    })
</script>
@endsection
