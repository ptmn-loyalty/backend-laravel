@extends('layouts.app')
@section('style')
<link href="{{ asset('css/dataTables/query.dataTables.min.css') }}" rel="stylesheet">
@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">List of Transactions</div>
                <div class="panel-body">
                    <table class="table table-bordered" id="transactions-table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>Produk</th>
                                <th>Jumlah (Liter / Kg / Kemasan)</th>
                                <th>Harga (IDR)</th>
                                <th>Poin</th>
                                <th>Waktu</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script src="{{ asset('js/dataTables/jquery.dataTables.min.js') }}" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#transactions-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{{ route('get-transactions') }}',
        columns: [
            { data: 'no', name: 'no' },
            { data: 'name', name: 'name' },
            { data: 'email', name: 'email' },
            { data: 'product_name', name: 'product_name' },
            { data: 'amount', name: 'amount' },
            { data: 'formatted_price', name: 'formatted_price' },
            { data: 'point', name: 'point' },
            { data: 'created_at', name: 'created_at' }
        ]
    });
});
</script>
@endsection