@extends('layouts.app')
@section('style')
<link href="{{ asset('css/dataTables/query.dataTables.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">List of SPBU</div>
                <div class="panel-body">
                    <table class="table table-bordered" id="spbus-table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode SPBU</th>
                                <th>Alamat</th>
                                <th>Kota</th>                                
                                <th>Jumlah Voucher Ditukarkan</th>                                
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script src="{{ asset('js/dataTables/jquery.dataTables.min.js') }}" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#spbus-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{{ route('get-spbus') }}',
        columns: [
            { data: 'no', name: 'no' },
            { data: 'spbu_code', name: 'spbu_code' },
            { data: 'address', name: 'address' },
            { data: 'city', name: 'city' },
            { data: 'voucher_count', name: 'voucher_count' }
        ]
    });
});
</script>
@endsection
