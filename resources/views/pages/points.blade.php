@extends('layouts.app')
@section('style')
<link href="{{ asset('css/dataTables/query.dataTables.min.css') }}" rel="stylesheet">
@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Points to Voucher</div>
                <div class="panel-body">
                    <table class="table table-bordered" id="transactions-table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>Produk</th>
                                <th>Poin</th>
                                <th>Dibeli Pada</th>
                                <th>Status</th>
                                <th>SPBU</th>
                                <th>Digunakan Pada</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script src="{{ asset('js/dataTables/jquery.dataTables.min.js') }}" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#transactions-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{{ route('get-users-vouchers') }}',
        columns: [
            { data: 'no', name: 'no' },
            { data: 'name', name: 'name' },
            { data: 'email', name: 'email' },
            { data: 'product_name', name: 'product_name' },
            { data: 'points_required', name: 'points_required' },
            { data: 'created_at', name: 'created_at' },
            { data: 'status', name: 'status' },
            { data: 'spbu_code', name: 'spbu_code' },
            { data: 'used_at', name: 'used_at' }
        ]
    });
});
</script>
@endsection
