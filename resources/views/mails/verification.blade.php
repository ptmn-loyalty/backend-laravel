<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Account Activation</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 600;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            .padding {
                padding-top: 10px;
                padding-left: 25px;
            }
        </style>
    </head>
    <body>
        <div class="padding">
            <p>
                Dear {{ $data->name }}, <br>
                Thank you for registering, <br>
                Below is Your Account information. Please keep it for future reference. <br>
                Username    : {{ $data->email }} <br>
                Please click the link below to activate your account.<br>
                <a href="{{ url('/user_verification/'.$data->email.'/'.$data->verification_code) }}" target="_blank">{{ env('APP_URL') }}/user_verification/{{ $data->email }}/{{ $data->verification_code }}</a> <br>
                <br>
                <br>
                Please note that Your Account data will not be saved to our database unless you activate it. If you can't click the link above, just try copy paste it to your browser. <br>
                <br>
                <br>
                Sincerely, <br>
                <br>
                PT. Pertamina
            </p>    
        </div>    
    </body>
</html>